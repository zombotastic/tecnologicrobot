package com.mygame.main;

import java.awt.Canvas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import java.util.Scanner;

import java.net.Socket;

public class game extends Canvas implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 640, HEIGHT = WIDTH / 12 * 9;
	
	public static int playerOneScore = 0;
	public static int playerTwoScore = 0;
	private Graphics g;
	
	
	private Thread thread;
	private boolean running = false;
	private Handler handler;
	private static Socket socket;
	static OutputStream output;
	static PrintWriter writer;
	static InputStream input;
	static BufferedReader reader;
	static String Dtype = "";
	static String valueIs = "";
	static boolean thisSwitch = true;
	Random r = new Random();
	HUD hud = new HUD();
	public game() {
		
		InetAddress ip = null;
		try {
			ip = InetAddress.getByName("10.0.0.233");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			socket = new Socket(ip, 20); 
			output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
            // writer.println("Hello World");
             /* 
             */
			input = socket.getInputStream();
			reader = new BufferedReader(new InputStreamReader(input));
			String datadata = reader.readLine();
			System.out.println(datadata);
			
           
			}
            catch (UnknownHostException ex) {
            	 
                System.out.println("Server not found: " + ex.getMessage());
     
            } catch (IOException ex) {
     
                System.out.println("I/O error: " + ex.getMessage());
            }
		//wait to connect
		//String PleaseWait = "";
		// while(PleaseWait == "")
			// {	
			// 	System.out.print("Try again!");
				
			//	try {
			//		if(reader.readLine() == null)
			//		{System.out.print("Try again!");}
			//		else { PleaseWait="Nice";}
			//	} catch (IOException e) {
					// TODO Auto-generated catch block
			//		e.printStackTrace();
			//	}
				
		//	}
				
		

		handler = new Handler();
		new Window(WIDTH, HEIGHT, "Let's Build PONG", this);	
		this.addKeyListener(new KeyInput(handler));
		
 		handler.addObject(new Ball(300,300,ID.Ball, handler));
		handler.addObject(new Player(50,100,ID.Player, handler));
		handler.addObject(new PlayerTwo(550,100,ID.PlayerTwo, handler));
		
		
	}
	
	public synchronized void start() {
		String PleaseWait = "";
		//Scanner scan = new Scanner(System.in);
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while (running) {
			
			long now = System.nanoTime();
			delta += (now - lastTime)/ ns;
			lastTime = now;
			while(delta >= 1) {
				tick();
				//
		
				delta--;
			}
			if(running)
				render();
			frames++;
			
			
			
			if(System.currentTimeMillis()-timer>1000) {
				this.addKeyListener(new KeyInput(handler));
				timer += 1000;
				System.out.println("FPS: " + frames);
				frames = 0;
			}
		}
		stop();
	}
	private void tick() {
		handler.tick();
		hud.tick();
		
		
	}	
	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
		
		g.setColor(Color.black);
		g.fillRect(0,0,WIDTH,HEIGHT);
		

		handler.render(g);
		hud.render(g);
		
		g.dispose();
		bs.show();
	}
	
	static void decipherDataStream(Handler handler) {
		
		try {
			if(reader.ready())
				if(thisSwitch == true)
				{	
					try {
						Dtype = reader.readLine();
						System.out.println(Dtype);
						thisSwitch = false;
						reader.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			else {
					if (Dtype.contains("PlayerTwoX"))
					{
						valueIs = reader.readLine();
						System.out.println(valueIs);
						reader.readLine();
						for(int i = 0; i < handler.object.size(); i++) {
							gameObject tempObject = handler.object.get(i);
							if(tempObject.getId() == ID.PlayerTwo) {
								tempObject.velX = Integer.parseInt(valueIs);
								}
							} 
					}
					if (Dtype.contains("PlayerTwoY"))
					{
						valueIs = reader.readLine();
						System.out.println(valueIs);
						reader.readLine();
						for(int i = 0; i < handler.object.size(); i++) {
							gameObject tempObject = handler.object.get(i);
							if(tempObject.getId() == ID.PlayerTwo) {
								tempObject.velY = Integer.parseInt(valueIs);
								}
							}
					}
					if (Dtype.contains("PlayerTwoScore"))
					{
						valueIs = reader.readLine();
						System.out.println(valueIs);
						reader.readLine();
						playerTwoScore += 1;
						for(int i = 0; i < handler.object.size(); i++) {
							gameObject tempObject = handler.object.get(i);
							if(tempObject.getId() == ID.Ball) {
								tempObject.y = HEIGHT/2;
								tempObject.x = WIDTH/2;
								tempObject.velX=5;
								tempObject.velY=3;
								}
							}
					}
					thisSwitch = true;
				}
			}
			catch(Exception e) {}
	}
		
	public static int clamp(int var, int min, int max) {
		if(var >= max) 
			return var = max;
		else if(var <= min)
			return var = min;
		else 
			return var;
	}
	

	public static void main (String[]args) {
		new game();
		// InetAddress ip = null;
		/**try {
			ip = InetAddress.getByName("192.168.1.108");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try(Socket socket = new Socket(ip, 20);) {
			OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.println("Hello World");
             /* 
             
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			 
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            	}	
			}
            catch (UnknownHostException ex) {
            	 
                System.out.println("Server not found: " + ex.getMessage());
     
            } catch (IOException ex) {
     
                System.out.println("I/O error: " + ex.getMessage());
            }
			 */
	}
		
}