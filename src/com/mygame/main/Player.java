package com.mygame.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Player extends gameObject {

	
	Random r = new Random();
	Handler handler;

	
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, 20, 45);
	}
	
	public void tick() {
	
		x += velX;
		//game.writer.println("PlayerOneX\n");
		//game.writer.println(String.valueOf(velX)+"\n");
		//game.writer.flush();
		y += velY;
		game.writer.println("PlayerOneY\n");
		game.writer.println(String.valueOf(velY)+"\n");
		game.writer.flush();
		
		y = game.clamp(y, 0, game.HEIGHT - 90);
		
		if (game.playerOneScore > 20)
		{
			System.out.print("Good Game! Player One Wins");
			System.exit(1);
			//game.GoodGame(g);					
		}
		collision();
		//game.decipherDataStream(handler);
		
	}
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			gameObject tempObject = handler.object.get(i);
			if(tempObject.getId() == ID.Ball) {
				if(getBounds().intersects(tempObject.getBounds())) {
					int tempVelX = tempObject.getVelX();
					int tempVelY = tempObject.getVelY();
					int tempPlayerY = getVelY();
					
					tempVelY = (tempVelY * -1) + tempPlayerY;
					tempVelX = tempVelX *= -1;
					tempVelY = tempVelY *= -1;
					tempObject.setVelX(tempVelX);
					tempObject.setVelY(tempVelY);
			}
		}
		}
	}
	
	public void render(Graphics g) {
	g.setColor(Color.white);
	g.fillRect(x, y, 20, 45);
		
	}



	
 

}
