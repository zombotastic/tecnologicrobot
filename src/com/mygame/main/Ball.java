package com.mygame.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Ball extends gameObject {

	private Handler handler;
	private int checkThis;

	public Ball(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		velX=5;
		velY=0;
		this.handler = handler;
		// TODO Auto-generated constructor stub
	}

	public void tick() {
		x += velX;
		y += velY;
		
	//	 if(checkThis == 40) {
	//	 game.writer.println("BallX\n");
	//	 game.writer.flush();
	//	 game.writer.println(String.valueOf(x)+"\n");
	//	 game.writer.flush();
	//	game.writer.println("BallY\n");
	//	game.writer.flush();
	//	game.writer.println(String.valueOf(y)+"\n");
	//	game.writer.flush();
	//	checkThis = 0;
	//	}
		checkThis += 1;
		if(y<= 0 || y >= game.HEIGHT - 32) velY*=-1;
		if(x<=0 || x >= game.WIDTH -32) velX*=-1;
		if(velX > 15)
		{velX = 15;}
		if(velY > 10)
		{velY = 10;}
		if (velY < -10)
		{velY = -10;}
		if(x == 0)
		{
			System.out.print("Point Player Two!");
			x = game.WIDTH/2;
			y = game.HEIGHT/2;
			setVelX(5);
			setVelY(3);
			//game.playerTwoScore += 1;
			//game.writer.println("PlayerTwoScore");
			//game.writer.flush();
			//game.writer.println(String.valueOf(game.playerTwoScore));
			//game.writer.flush();
			
			//game.decipherDataStream();
			handler.addObject(new Text(40, 40, ID.Text));
			
			//setVelX(-3);

		}
		if (x == game.WIDTH - 35)
		{
			System.out.print("Point Player One!");
			x = game.WIDTH/2;
			y = game.HEIGHT/2;
			setVelX(-5);
			setVelY(-3);
			game.playerOneScore += 1;
			game.writer.println("PlayerOneScore\n");
			game.writer.flush();
			game.writer.println(String.valueOf(game.playerOneScore)+"\n");
			game.writer.flush();
			handler.addObject(new Text(40, 40, ID.Text));
			//game.decipherDataStream(handler);
			//setVelX(3);
		}
	}
		

	
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x, y, 16, 16);
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, 16, 16);
	}
 

}
