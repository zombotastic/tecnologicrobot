package com.mygame.main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyInput extends KeyAdapter {
	private Handler handler;
	public KeyInput(Handler handler) {
		this.handler = handler;
	}	
	
	public void keyPressed(KeyEvent e) {  

		int key = e.getKeyCode();
		for(int i = 0; i < handler.object.size(); i++) {
			
		gameObject tempObject = handler.object.get(i);
		
		if(tempObject.getId() == ID.Player)
		{
			
			if(key == KeyEvent.VK_W) {tempObject.setVelY(-7);}
			if(key == KeyEvent.VK_S) {tempObject.setVelY(7);}
			
		}
		/**if(tempObject.getId() == ID.PlayerTwo)
		{
			if(key == KeyEvent.VK_UP) {tempObject.setVelY(-7);}
			if(key == KeyEvent.VK_DOWN) {tempObject.setVelY(7);}
			
		}
		
		*/
		}
		if(key == KeyEvent.VK_ESCAPE) System.exit(1);
		}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		for(int i = 0; i < handler.object.size(); i++) {
			
			gameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Player)
			{
				if(key == KeyEvent.VK_W) tempObject.setVelY(0);
				if(key == KeyEvent.VK_S) tempObject.setVelY(0);
			//	if(key == KeyEvent.VK_A) tempObject.setVelX(0);
			//	if(key == KeyEvent.VK_D) tempObject.setVelX(0);
			}/**
			if(tempObject.getId() == ID.PlayerTwo)
			{
				if(key == KeyEvent.VK_UP) {tempObject.setVelY(0);}
				if(key == KeyEvent.VK_DOWN) {tempObject.setVelY(0);}
				
			}*/
		}
	}
}