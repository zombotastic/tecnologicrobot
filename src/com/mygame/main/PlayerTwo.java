package com.mygame.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class PlayerTwo extends gameObject {

Handler handler;

	
	public PlayerTwo(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, 20, 45);
	}
	
	public void tick() {
		game.decipherDataStream(handler);
		x += velX;
		/**game.writer.println("PlayerTwoX");
		game.writer.flush();
		game.writer.println("E"+String.valueOf(x));
		game.writer.flush();
		*/
		y += velY;
		/**game.writer.println("PlayerTwoY");
		game.writer.flush();
		game.writer.println("F"+String.valueOf(y));
		game.writer.flush();
		*/
		//game.decipherDataStream();
		if (game.playerTwoScore > 7)
		{
			System.out.print("Good Game! Player Two Wins");
			System.exit(1);
			//game.GoodGame(g);
		}
		
		y = game.clamp(y, 0, game.HEIGHT - 90);
		
		game.decipherDataStream(handler);
		game.decipherDataStream(handler);
		
		collision();
			
		
	}
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			gameObject tempObject = handler.object.get(i);
			if(tempObject.getId() == ID.Ball) {
				if(getBounds().intersects(tempObject.getBounds())) {
					int tempVelX = tempObject.getVelX();
					int tempVelY = tempObject.getVelY();
					int tempPlayerY = getVelY();
					
					tempVelY = tempVelY + tempPlayerY;
					tempVelX = tempVelX *= -1;
					tempVelY = tempVelY *= -1;
					tempObject.setVelX(tempVelX);
					tempObject.setVelY(tempVelY);
			}
		}
		}
	}
	
	public void render(Graphics g) {
	g.setColor(Color.white);
	g.fillRect(x, y, 20, 45);
		
	}

}
